#!/bin/sh 
DEMO="AppDev Cloud Retail Web Shop Demo"
AUTHORS="Andrew Block, Eric D. Schabell, Duncan Doyle"
PROJECT="git@gitlab.com:redhatdemocentral/crc-retail-webshop-demo.git"
SRC_DIR=./installs
OC_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable/"

# Adjust these variables to point to an OCP instance.
OPENSHIFT_USER=developer
OPENSHIFT_PWD=developer
HOST_IP=api.crc.testing   # set with OCP instance hostname or IP.
HOST_APPS=apps-crc.testing
HOST_PORT=6443
OCP_APP=retail-webshop-demo
OCP_PRJ=appdev-in-cloud
FRONT_END_APP=webshop-app

# rhpam container configuraiton.
KIE_ADMIN_USER=erics
KIE_ADMIN_PWD=redhatpam1!
MEM_LIMIT=2Gi
VERSION=77

# rewards project details.
PRJ_ID=retail-webshop
PRJ_REPO="https://gitlab.com/bpmworkshop/rhdm-retail-webshop-repo.git"
DELAY=300   # waiting max 5 min various container functions to startup.

# import container functions.
source support/container-functions.sh

# wipe screen.
clear 

echo
echo "###################################################################"
echo "##                                                               ##"   
echo "##  Setting up the ${DEMO}         ##"
echo "##                                                               ##"   
echo "##                                                               ##"   
echo "##  ####  #   # ####  #   #        #### #      ###  #   # ####   ##"
echo "##  #   # #   # #   # ## ##   #   #     #     #   # #   # #   #  ##"
echo "##  ####  ##### #   # # # #  ###  #     #     #   # #   # #   #  ##"
echo "##  # #   #   # #   # #   #   #   #     #     #   # #   # #   #  ##"
echo "##  #  #  #   # ####  #   #        #### #####  ###   ###  ####   ##"
echo "##                                                               ##"   
echo "##  brought to you by,                                           ##"   
echo "##             ${AUTHORS}      ##"
echo "##                                                               ##"   
echo "##  ${PROJECT}  ##"
echo "##                                                               ##"   
echo "###################################################################"
echo

# check for passed target IP.
if [ $# -eq 1 ]; then
	echo "Checking for host ip passed as command line variable."
	echo
	if valid_ip "$1" || [ "$1" == "$HOST_IP" ]; then
		echo "OpenShift host given is a valid IP..."
		HOST_IP=$1
		echo
		echo "Proceeding with OpenShift host: $HOST_IP..."
		echo
	else
		# bad argument passed.
		echo "Please provide a valid IP that points to an OpenShift installation..."
		echo
		print_docs
		echo
		exit
	fi
elif [ $# -gt 1 ]; then
	print_docs
	echo
	exit
elif [ $# -eq 0 ]; then
	# validate HOST_IP.
  if [ -z ${HOST_IP} ]; then
	  # no host name set yet.
	  echo "No host name set in HOST_IP..."
	  echo
		print_docs
		echo
		exit
	else
		# host ip set, echo and proceed with hostname.
		echo "You've manually set HOST to '${HOST_IP}' so we'll use that for your OpenShift Container Platform target."
		echo
	fi
fi

# make some checks first before proceeding.	
command -v oc version --client >/dev/null 2>&1 || { echo >&2 "OpenShift CLI tooling is required but not installed yet... download here (unzip and put on your path): ${OC_URL}"; exit 1; }
echo "OpenShift command line tools installed... checking for valid version..."
echo

echo "OpenShift commandline tooling is installed..."
echo 
echo "Logging in to OpenShift as $OPENSHIFT_USER..."
echo
oc login ${HOST_IP}:${HOST_PORT} --password=$OPENSHIFT_PWD --username=$OPENSHIFT_USER

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc login' command!"
	exit
fi

echo
echo "Creating a new project..."
echo
oc new-project "$OCP_PRJ"

if [ "$?" -ne "0" ]; then
	echo
	echo "It lookes like the project already exists..."
fi

echo 
echo "Installing front-end application for web shop..."
echo
echo "Check for availability of correct version of Red Hat EAP basic s2i template..."
echo
oc get templates -n openshift eap${EAP_VERSION}-basic-s2i >/dev/null 2>&1

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc get template eap-basic-s2i'' command!"
	echo
	echo "Your container platform is mising this template version in your catalog: eap${EAP_VERSION}-basic-s2i"
	echo "Make sure you are using the correct version of Code Ready Containers as listed in project Readme file."
	echo
	exit
fi

echo
echo "Check for availability of correct version of Red Hat Decision Manager Authoring template..."
echo
oc get templates -n openshift rhdm${VERSION}-authoring >/dev/null 2>&1


if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc get template rhdm-authoring' command!"
	echo
	echo "Your container platform is mising this template version in your catalog: rhdm${VERSION}-authoring"
	echo "Make sure you are using the correct version of Code Ready Containers as listed in project Readme file."
	echo
	exit
fi

echo
echo "Setting up a secrets and service accounts..."
echo
oc process -f support/app-secret-template.yaml -p SECRET_NAME=decisioncentral-app-secret | oc create -f -
oc process -f support/app-secret-template.yaml -p SECRET_NAME=kieserver-app-secret | oc create -f -

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc process' command!"
	echo
	exit
fi

echo
echo "Setting up secrets link for kieserver user and password..."
echo
oc create secret generic rhpam-credentials --from-literal=KIE_ADMIN_USER=${KIE_ADMIN_USER} --from-literal=KIE_ADMIN_PWD=${KIE_ADMIN_PWD}

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc secrets' creating kieserver user and password!"
	echo
	exit
fi

echo
echo "Processing to setup KIE-Server with CORS support..."
echo
oc process -f ${SUP_DIR}/rhdm${VERSION}-kieserver-cors.yaml \
	-p DOCKERFILE_REPOSITORY="https://gitlab.com/redhatdemocentral/crc-quick-loan-bank-demo.git" \
	-p DOCKERFILE_REF="master" \
	-p DOCKERFILE_CONTEXT=${SUP_DIR}/rhdm${VERSION}-kieserver-cors \
	| oc create -f -

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc process' cors kie-server command!"
	exit
fi

echo
echo "Creating a new application using CRC RHDM catalog image..."
echo
oc new-app --template=rhdm$VERSION-authoring \
	-p APPLICATION_NAME="$OCP_APP" \
  -p DECISION_CENTRAL_HTTPS_SECRET="decisioncentral-app-secret" \
  -p KIE_SERVER_HTTPS_SECRET="kieserver-app-secret" \
	-p CREDENTIALS_SECRET="rhpam-credentials" \
	-p MAVEN_REPO_USERNAME="$KIE_ADMIN_USER" \
	-p MAVEN_REPO_PASSWORD="$KIE_ADMIN_PWD" \
  -p DECISION_CENTRAL_VOLUME_CAPACITY="$PV_CAPACITY"

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc new-app' rhdm command!"
	exit
fi

echo
echo "Setting up old openshift controller strategy..."
echo
# Disable the OpenShift Startup Strategy and revert to the old Controller Strategy
oc set env dc/${OCP_APP}-rhdmcentr KIE_WORKBENCH_CONTROLLER_OPENSHIFT_ENABLED=false
oc set env dc/${OCP_APP}-kieserver KIE_SERVER_STARTUP_STRATEGY=ControllerBasedStartupStrategy KIE_SERVER_CONTROLLER_USER=${KIE_ADMIN_USER} KIE_SERVER_CONTROLLER_PWD=${KIE_ADMIN_PWD} KIE_SERVER_CONTROLLER_SERVICE=${OCP_APP}-rhdmcentr KIE_SERVER_CONTROLLER_PROTOCOL=ws  KIE_SERVER_ROUTE_NAME=insecure-${OCP_APP}-kieserver

echo
echo "Patch the KIE-Server name to use CORS support..."
echo
oc patch dc/${OCP_APP}-kieserver --type='json' -p="[{'op': 'replace', 'path': '/spec/triggers/0/imageChangeParams/from/name', 'value': 'rhdm${VERSION}-kieserver-cors:latest'}]"

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc patch' kie-server name cors support command!"
	exit
fi

echo
echo "Patch the KIE-Server namespace to use CORS support..."
echo
oc patch dc/${OCP_APP}-kieserver --type='json' -p="[{'op': 'replace', 'path': '/spec/triggers/0/imageChangeParams/from/namespace', 'value': '${OCP_PRJ}'}]"

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc patch' kie-server namespace cors support command!"
	exit
fi

if container_ready; then
	echo
	echo "The container has started..."
	echo
else
	echo "Exiting now with CodeReady Container started, but not sure if "
	echo "authoring environment is ready and did not install the demo project."
	echo
	exit
fi

echo "Creating a space for the project import..."
echo

if create_project_space; then
  echo "Creation of new space for project import started..."
  echo
else
  echo "Exiting now with CodeReady Container started, but not sure if "
  echo "authoring environment is ready and did not install the demo project."
	echo 
	exit
fi

echo "Validating new project space creation..."
echo

if validate_project_space; then
	echo "Creation of space successfully validated..."
	echo
else
	echo "Exiting now with CodeReady Container started, autorhing environment"
	echo "is ready, but unable to import the demo project."
	echo
	exit
fi

echo "Checking if project already exists, otherwise add it..."
echo

if project_exists; then
	echo "Demo project already exists..."
	echo 
else
	echo "Project does not exist, importing in to container..."
	echo
	
	if project_imported; then
		echo "Imported project successfully..."
		echo
	else
	  echo "Exiting now with Code ReadyContainer started, authoring environment"
	  echo "is ready, but unable to import the demo project."
	  echo
		exit
	fi
fi

echo
echo "Creating a new front-end webshop application using CRC catalog image..."
echo
oc new-app --template=eap${EAP_VERSION}-basic-s2i \
	-p SOURCE_REPOSITORY_URL="https://gitlab.com/redhatdemocentral/rhcs-retail-webshop-demo.git" \
	-p SOURCE_REPOSITORY_REF="master" \
	-p CONTEXT_DIR="projects/webstore-demo" \
  -p APPLICATION_NAME="${FRONT_END_APP}"  \
	-p MAVEN_ARGS_APPEND="-Dkie.maven.settings.custom=/tmp/src/settings.xml"

if [ "$?" -ne "0" ]; then
	echo
	echo Error occurred during 'oc new-app' command!
	exit
fi

echo
echo "Creating config-map for client application..."
echo
oc create configmap ${FRONT_END_APP}-config-map --from-file=${PRJ_DIR}/application-ui/config/config.js

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc create' config-map command!"
	exit
fi



echo
echo "================================================================================="
echo "=                                                                               ="
echo "=  Login to Red Hat Decision Manager to exploring process automation            ="
echo "=  development at:                                                              ="
echo "=                                                                               ="
echo "=   https://${OCP_APP}-rhdmcentr-${OCP_PRJ}.${HOST_APPS} ="
echo "=                                                                               ="
echo "=    Log in: [ u:erics / p:redhatdm1! ]                                         ="
echo "=                                                                               ="
echo "=    Others:                                                                    ="
echo "=            [ u:kieserver / p:redhatdm1! ]                                     ="
echo "=                                                                               ="
echo "=  After deploying the webshop project in Red Hat Decision Manager              ="
echo "=  you can access the webshop front-end and start shopping here:                ="
echo "=                                                                               ="
echo "=   https://${FRONT_END_APP}-${OCP_PRJ}.${HOST_APPS}                        ="
echo "=                                                                               ="
echo "=  Note: it takes a few minutes to expose the services.                         ="
echo "=                                                                               ="
echo "================================================================================="
echo

